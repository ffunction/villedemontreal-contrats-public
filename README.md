#  Ville de Montréal
## Contrats

```
Version : 1.0.1
URL     : http://ffctn.com/a/contrats
Git     : https://bitbucket.org/ffunction/villedemontreal-contrats-public
Updated : 2015-04-24
```

### Data

The data is collected from a custom API provided by OpenNorth and available
(as staging) at <http://ovc-stage.herokuapp.com/>.

#### API

The API allows to query the City of Montréal contracts as an Open-Contracting
compliant JSON format. You can have glimpse of the data by opening the
following URL:

<http://ovc-stage.herokuapp.com/api/releases>

##### Contracts

*Contracts* are defined mainly by the following properties:

- `id:string`, the contract `id` as a string (but in practice, a stringified int)
- `ocid:string`, the open contracting id for the contract, as a string.
- `date:"YYYY-MM-DDTHH:MM:SS[-+]HH:MM"` as a string in ISO date time with delta to UTC
- `tag:string`, can be `award` or... TBD
- `tender:Tender`, see below. Note that you should use values from awards first as they
   override the ones defined in `tender`.
- `buyer:Buyer`, see below
- `awards:Award`, see below
- `language:String` the language code, usually `"fr"`


Note the following values of interest:

- `repartition` as `awards.repartition`
- `direction` (ie. the parent organisation of services) as `buyerl.suborganizationof.name`
- `decision number` as `awards[0].items[0].id`
- `case number` as `awards[0].id`

```
{
	"awards":[...],
	"buyer":{...},
	"date": "2012-12-20T00:00:00-05:00",
	"id": "1124338001",
	"language": "fr",
	"ocid": "ocds-a1234567-mt-20ba41bab435952d12c63ddf03b3365ac71f5687",
	"tag": "award",
	"tender":{...},
}
```

##### Tenders

*Tenders* are defined bu the following properties:

- `title:string` the title for the tender
- `description:string` the description for the tender. They are in French
  and oddly enough, in ALL-CAPS.
- `id:string` a string identifier for the tender
- `status:[complete|...TBD]` the status of the tender
- `value:int` the value for the contract
- `items:[Item]` a list of items with the following properties
	- `id:string` seems to be the same as the parent tender's `id`
	- `quantity:int` the quantity of items
	- `description:string` a usually ALL-CAPS string
- `procuringEntity` which has the following properties:
	- `address` as `{countryName,locality,postalCode,region,streeAddress}`
	- `name:string`

```
{
	"description": "AUTORISER LA PROLONGATION POUR UNE PÉRIODE ADDITIONNELLE DE
	12 MOIS SE TERMINANT LE 31 DÉCEMBRE 2013, DES ENTENTES-CADRES CONCLUES AVEC
	LÉCUYER & FILS LTÉE, RÉAL HUOT INC. ET WOLSELEY CANADA INC., POUR LA
	FOURNITURE SUR DEMANDE DE PIÈCES EN FONTE POUR AQUEDUC ET ÉGOUT, TUBES EN
	CUIVRE, TUYAUX ET RACCORDS EN PVC POUR ÉGOUT (CM09 0010), SUITE À L'APPEL
	D'OFFRES PUBLIC # 08-10712. ",
	"id": "CM12 1096",
    "items":[
		{
			"description": "DIRECTION DE L'APPROVISIONNEMENT. ENTENTES CADRES RÉPARTITION EN FONCTION DE LA CONSOMMATION",
			"id": "CM12 1096",
			"quantity": 1
		}
	],
	"procuringEntity": {
		"address":{
				"countryName": "Canada",
				"locality": "Montréal",
				"postalCode": "H2Y 1C6",
				"region": "QC",
				"streetAddress": "275 Rue Notre-Dame Est, Montréal"
			},
			"name": "Conseil municipal"
		},
	"status": "complete",
	"title": "Autre",
	"value": 0
}
```

##### API URLs

`/api/releases` [⎇](http://ovc-stage.herokuapp.com/api/releases)

:	Allows to query the contracts without a specific grouping.

	-  `format=[json|csv|pdf|ocds|xslx]` allows to get the results under
	   the specific format. `json` is the default.

	Example

	- list of all  contracts <http://ovc-stage.herokuapp.com/api/releases?limit=10000000>

`/api/releases/by_supplier` [⎇](http://ovc-stage.herokuapp.com/api/releases/by_supplier?limit=10000000)

:	Returns a list of contracts grouped by supplier

`/api/releases/by_buyer` [⎇](http://ovc-stage.herokuapp.com/api/releases/by_buyer?limit=10000000)

:	Returns a list of contracts grouped by buyer

`/api/releases/by_value?bucket={min:int},{max:int},{segments:int}`

:	Returns a list of contracts grouped by amounts. Amounts will be
	aggregated according to buckets specified as an increasing
	semicolon-separated list of values.

	- `min` is the minimum contract amount
	- `max` is the maximum contract amount
	- `segments` is the number of quantiles

	Ex:

	- [api/releases/by_value=bucket=0,100000,49](http://ovc-stage.herokuapp.com/api/releases/by_value?bucket=0,1000000,49)

`api/by_procuring_entity` [⎇](http://ovc-stage.herokuapp.com/api/releases/by_procuring_entity?limit=10000000)

:	Return grouped of contracts by procuring entity


`/api/releases/by_month` [⎇](http://ovc-stage.herokuapp.com/api/releases/by_month?limit=10000000)

:	Returns groups of contracts by month

`/api/releases/by_activity` [⎇](http://ovc-stage.herokuapp.com/api/releases/by_activity?limit=10000000)

:	Returns groups of contracts by activity. Activity refers to the specific
	main "theme" of the contract (transport, security, etc)

`/api/release/{OCID}`

:	Retrieves the information for the given contract, using the open contracting
	id for the contract (this looks like `ocds-aXXXXXXX-mt-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX`).

	Note that the OCDS ids are not stable -- they can be regenerated and not
	match the same contract, or even not resolve at all.

`/api/treemap?parent=activity&child=buyer` [⎇](http://ovc-stage.herokuapp.com/api/treemap?parent=activity&child=buyer)

:	Retrives the information to be used in the treemap visualization, presenting
	a breakdown of buyers per activity.

`/api/treemap?parent=buyer&child=supplier&activity=Infrastructures` [⎇](http://ovc-stage.herokuapp.com/api/treemap?parent=buyer&child=supplier&activity=Infrastructures)

:	Retrives the information to be used in the treemap visualization, presenting
	a breakdown of suppliers by buyer within the given activity.

#### API Parameters

The queries all support the following options:

`q=<string>`

:	full-text search of the given query string.

`value_[gt|lt]=<amount>`

:	queries contract with an amount greater or lower than the given threshold, inclusively.

`date_[gt|lt]=<YYYY-MM-DD>`

:	queries contract with an amount greater or lower than the given date threshold, inclusively.

`buyer=<buyer id/slug>,...`

:	restricts results to the ones featuring the given buyer. Takes a semicolon-separated
	list of buyers.

`supplier=<supplier id/slug>;...`

:	restricts results to the ones featuring the given supplier. Takes a semicolon-separated
	list of suppliers.

`supplier_size=[1|2|3];...`

:	restricts results to suppliers of the given size. Takes a semicolon-separated
	list of tiers.

`procuring_entity=<procuring entity id/slug>;...`

:	restricts results to the ones featuring the given procuring entity. Takes a
	semicolon-separated list of tiers.

`activity=<string>;...`

:	restricts results to the given activity. Takes a
	semicolon-separated list of activities.  Note that activities have no
	"slug", so you can use the activity string as-is.

`order_by:[value|id|date|buyer]`

:	Sorts the result by contract `value`, contract `id`, `date` or `buyer`

`limit=<integer=50>`

:	Limits the number of matching contracts, 50 by default.

`offset=<integer=0>`

:	Offset in paginated search, 0 by default.

Libraries
=========

This application uses [jQuery](http://jquery.com/)  ([MIT
License](https://jquery.org/license/)). All other code is copyright
[FFunction](http://ffctn.com).

License
=======

Copyright (c) 2015, [FFunction](http://ffctn.com) (1165373771 Québec inc.) All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer. Redistributions in binary form
must reproduce the above copyright notice, this list of conditions and the
following disclaimer in the documentation and/or other materials provided with
the distribution. Neither the name of the FFunction inc (CANADA) nor the names
of its contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
